<?php

require_once 'users/init.php';
require_once $abs_us_root.$us_url_root.'users/includes/template/prep.php';
if(isset($user) && $user->isLoggedIn()){
}
?>

<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<link rel="stylesheet" href="<?=$us_url_root?>app/css/text_background.css">
<link rel="stylesheet" href="<?=$us_url_root?>app/css/peeled.css">

<div id="page-wrapper">

        <!-- Slide1 -->
        <section id="main_section_1" class="section-slide" style="background-color: black">
            <div class="wrap-slick1">
                <div class="slick1">
                    <div class="item-slick1 item1-slick1" style="background-image: url(<?=$us_url_root?>usersc/templates/<?=$settings->template?>/assets/images/medical1.png);">
                        <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<span class="caption1-slide1 txt1 t-center animated visible-false m-b-15" data-appear="fadeInDown">
							Bem vindo ao
						</span>

                            <h2 class="caption2-slide1 tit1 t-center animated visible-false m-b-37" data-appear="fadeInUp">
                                PEP Educ
                            </h2>

                            <div class="wrap-btn-slide1 animated visible-false" data-appear="zoomIn">
                                <!-- Button1 -->
                                <a href="" class="btn1 flex-c-m size1 txt3 trans-0-4">
                                    Engajamento
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="item-slick1 item2-slick1" style="background-image: url(<?=$us_url_root?>usersc/templates/<?=$settings->template?>/assets/images/medical2.png);">
                        <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<span class="caption1-slide1 txt1 t-center animated visible-false m-b-15" data-appear="rollIn">
							Um sistema de ensino do
						</span>

                            <h2 class="caption2-slide1 tit1 t-center animated visible-false m-b-37" data-appear="lightSpeedIn">
                                Laboratório de Simulação UFF
                            </h2>

                            <div class="wrap-btn-slide1 animated visible-false" data-appear="slideInUp">
                                <!-- Button1 -->
                                <a href="" class="btn1 flex-c-m size1 txt3 trans-0-4">
                                    Engajamento
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="item-slick1 item3-slick1" style="background-image: url(<?=$us_url_root?>usersc/templates/<?=$settings->template?>/assets/images/medical3.png);">
                        <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<span class="caption1-slide1 txt1 t-center animated visible-false m-b-15" data-appear="rotateInDownLeft">
							Direcionado à
						</span>

                            <h2 class="caption2-slide1 tit1 t-center animated visible-false m-b-37" data-appear="rotateInUpRight">
                                Medicina Digital
                            </h2>

                            <div class="wrap-btn-slide1 animated visible-false" data-appear="rotateIn">
                                <!-- Button1 -->
                                <a href="" class="btn1 flex-c-m size1 txt3 trans-0-4">
                                    Engajamento
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <?php  //languageSwitcher();?>

</div>

<!-- Place any per-page javascript here -->


<?php require_once $abs_us_root . $us_url_root . 'users/includes/html_footer.php'; ?>
