<?php
/**
 * Created by PhpStorm.
 * User: robso
 * Date: 18/02/2021
 * Time: 16:00
 */

require_once '../users/init.php';
require_once $abs_us_root.$us_url_root.'users/includes/template/prep.php';
if(isset($user) && $user->isLoggedIn()){

    require_once 'globals.php';
    require_once 'functions.php';
    include "reg/forms.html";

    $query_nrs = getNRs();

    $nrs = $query_nrs->results();

?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/styles.css">

    <div class="container-fluid">
        <nav class="navbar" style="padding: 15px">
            <ul class="nav">
                <?php if (hasPerm([USER_PERM], $user->data()->id)) { ?>
                    <li>
                        <a href="#window_nr_reg" class="list-item" rel="modal" onclick="clickNewNR()">
                            <i class="fa fa-edit fa-2x" style="padding-top: 5px"></i>
                            <p>Ação número 1</p>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </nav>
    </div>
    <div id="mask"></div>

<?php
} else {
    ?>

    Você não está logado no site!

    <?php
}
?>

<!-- Place any per-page javascript here -->
<?php require_once $abs_us_root . $us_url_root . 'usersc/templates/' . $settings->template . '/footer.php'; //custom template footer ?>

