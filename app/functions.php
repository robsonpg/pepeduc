<?php
/**
 * Created by PhpStorm.
 * User: Robson
 * Date: 28/10/2019
 * Time: 08:27
 */

require_once "globals.php";

/*
 * #####################################################################################################################
 * Funções para controle do cadastro de NRs
 * #####################################################################################################################
 */

function getNRs() {
    $db = DB::getInstance();

    // GET USERS
    $get_clients = $db->query("SELECT * FROM SP_rs WHERE SP_rs_delete_date is null order by SP_rs_name asc");

    return $get_clients;
}

function getNR($id) {
    $db = DB::getInstance();

    // GET USER
    $get_client = $db->query("SELECT * FROM device_clients WHERE idclient = " . $id . " and client_delete_date is null");

    return $get_client;
}

function saveNR($NRfullname, $NRname, $NRkeysnumber) {

    $db = DB::getInstance();

    // INSERT company in table
    $sql = "INSERT INTO SP_rs (SP_rs_name, SP_rs_fullname, SP_rs_keys, SP_rs_insert_date) VALUES (\"" . $NRname . "\", \"" . $NRfullname  .
        "\", \"" . $NRkeysnumber . "\" , now())";

    $insert_result = $db->query($sql);

    return $db->errorString();
    //return $insert_result;
}

function deleteNR($id) {
    $db = DB::getInstance();

    // preenche a data de deleção
    $sql = "UPDATE SP_rs SET SP_rs_delete_date = now() WHERE idSP_rs=" . $id;

    $delete_result = $db->query($sql);

    return $db->errorString();
}

function updateNR($id, $name, $street, $number, $district, $city, $state, $country, $postalcode, $phone, $CPF, $email, $type,
    $gender) {

    $db = DB::getInstance();

    // INSERT company in table
    $sql = "UPDATE device_clients SET client_name='" . $name . "', client_street='" . $street . "', client_house_number='" .
        $number . "', client_district='" . $district . "', client_city='" . $city . "', client_state='" .
        $state . "', client_country='" . $country . "', client_postcode='" . $postalcode . "', client_phone='" . $phone .
        "', client_CPF='" . $CPF . "', client_email='" . $email ."', client_type='" . $type . "', client_gender=" .
        $gender . " WHERE idclient=" . $id;

    $update_result = $db->query($sql);

    return $update_result;
}




