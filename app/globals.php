<?php
/**
 * Created by PhpStorm.
 * User: robso
 * Date: 22/02/2021
 * Time: 17:52
 */

// Constantes das permissões de usuário
define('USER_PERM', '1');
define('ADMIN_PERM', '2');
define('MANAGER_PERM', '3');
define('WORKER_PERM', '4');
define('DEV_PERM', '5');
define('TEST_PERM', '666');


// códigos de erro

//ERROR #23000 - Unique ID
define('ERROR_UNIQUE_ID', '23000');