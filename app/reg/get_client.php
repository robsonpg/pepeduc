<?php
/**
 * Created by PhpStorm.
 * User: Robson
 * Date: 19/11/2019
 * Time: 11:17
 */

require_once '../../users/init.php';
//require_once $abs_us_root.$us_url_root.'users/includes/template/prep.php';

if(isset($user) && $user->isLoggedIn()) { } else { header( "Location: /index.php" ); exit; }
if (!securePage($_SERVER['PHP_SELF'])){die();}

require_once '../functions.php';

$id = $_GET['id'];


//echo $name;

    $result = getClient($id);

    $get_client_count = $result->count();

    // Verifica se a consulta retornou linhas
    if ($get_client_count > 0) {
        $client_result = $result->results();
        //dump($user_result);
        echo json_encode($client_result);
    } else {
        // Se a consulta não retornar nenhum valor, exibi mensagem para o usuário
        echo "Não foram encontrados registros!";
    }

?>