/**
 * Created by robso on 23/02/2021.
 */

/*
 #######################################################################################
 Check if fields filled
 #######################################################################################
 */

function checkRegisterFormFields() {
    // reference all fields
    edit_fullname = document.getElementById("frm_NR_fullname");
    edit_NR_name = document.getElementById("frm_NR_name");
    edit_NR_keysearch = document.getElementById("frm_NR_keysearch");

    // verify field empty
    //alert("'" + edit_name.value + "'");

    if (!shakeDOM(edit_fullname)) return false;
    if (!shakeDOM(edit_NR_name)) return false;
    if (!shakeDOM(edit_NR_keysearch)) return false;

    return true;
}

/*
 #######################################################################################
 Sinaliza a necessidade de preenchimento
 #######################################################################################
 */
function shakeDOM(element) {
    if (element.value == '') {
        //alert("animation!");
        element.style.borderColor = "red";
        element.className = "shake";
        element.addEventListener("webkitAnimationEnd", function endEdit() {
            element.style.borderColor = "#A9A9A9";
            element.className = "";
            element.focus();
        });

        return false;
    }
    return true;
}

function clickNewNR() {

    // Limpa os campos
    document.getElementById("frm_NR_fullname").value = '';
    document.getElementById("frm_NR_name").value = '';
    document.getElementById("frm_NR_keysearch").value = '';
    document.getElementById("frm_NR_fullname").focus();
}

/*
 #######################################################################################
 Preenche os campos para edição
 #######################################################################################
 */
function editNR(id, nr_name, nr_full_name, nr_key) {

    //alert("Click edit: " + id);
    // reference all fields
    var edit_fullname = document.getElementById("frm_NR_fullname");
    var edit_NR_name = document.getElementById("frm_NR_name");
    var edit_NR_keysearch = document.getElementById("frm_NR_keysearch");
    edit_NR_name.value = nr_name;
    edit_fullname.value = nr_full_name;
    edit_NR_keysearch.value = nr_key;
    edit_fullname.focus();
}

/*
 #######################################################################################
 Inicio das ações depois da carga da página
 #######################################################################################
 */

$(document).ready(function() {


    /// Quando usuário clicar em salvar e fechar será feito todos os passo abaixo
    $('#save_close_NR').click(function() {

        var data = $('#form-inline').serialize();

        // verify fields
        if (!checkRegisterFormFields()) return;

        //alert(data);
        //return;

        ajaxRequest = $.ajax({
            type: 'GET',
            url: 'reg/save_nr.php',
            //dataType: "JSON",
            //async: true,
            data: data,
            processData: false,
            contentType: false,
            success: function(response) {
                //location.reload();
                if (response.indexOf("ERRO") !== -1) {
                    alert(response);
                } else {
                    document.getElementById("lbl_success").style.display = "block";
                    setTimeout(function() {
                        document.getElementById("lbl_success").style.display = "none";
                        $("#mask").hide();
                        $("#window_nr_reg").hide();
                        location.reload();
                    }, 1000);
                    //$("#mask").hide();
                    //$("#window_reg").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Error: " + textStatus + " - " + errorThrown);
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    /// Quando usuário clicar em salvar e continuar será feito todos os passo abaixo
    $('#save_cont_NR').click(function() {

        var data = $('#form-inline').serialize();

        // verify fields
        if (!checkRegisterFormFields()) return;

        //alert(data);
        //return;

        ajaxRequest = $.ajax({
            type: 'GET',
            url: 'reg/save_nr.php',
            //dataType: "JSON",
            //async: true,
            data: data,
            processData: false,
            contentType: false,
            success: function(response) {
                //location.reload();
                if (response.indexOf("ERRO") !== -1) {
                    alert(response);
                } else {
                    // Limpa os campos
                    document.getElementById("frm_NR_fullname").value = '';
                    document.getElementById("frm_NR_name").value = '';
                    document.getElementById("frm_NR_keysearch").value = '';
                    document.getElementById("frm_NR_fullname").focus();
                    document.getElementById("lbl_success").style.display = "block";
                    setTimeout(function() {
                        document.getElementById("lbl_success").style.display = "none";
                    }, 1000);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Error: " + textStatus + " - " + errorThrown);
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    $('#update').click(function() {

        var data = 'id=' + reg_id + '&' + $('#formRegister').serialize();
        //alert(data);

        // verify fields
        if (!checkRegisterFormFields()) return;

        ajaxRequest = $.ajax({
            type: 'GET',
            url: 'update_client.php',
            //dataType: "JSON",
            //async: true,
            data: data,
            processData: false,
            contentType: false,
            success: function(response) {
                $("#mask").hide();
                $(".window").hide();
                $(".deletewindow").hide();
                getClientData();
                //location.reload();
                //alert(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Error: " + textStatus + " - " + errorThrown);
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    $('#confirm').click(function () {

        var data = 'id=' + reg_id;

        //alert(data);

        ajaxRequest = $.ajax({
            type: 'GET',
            url: 'delete_client.php',
            //dataType: "JSON",
            //async: true,
            data: data,
            processData: false,
            contentType: false,
            success: function(response) {
                location.reload();
                //(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Error: " + textStatus + " - " + errorThrown);
                console.log(textStatus, errorThrown);
            }
        });
    })

    $('#cancel_NR').click( function () {
        //alert('Canceled!');
        $("#window_nr_reg").hide();
        $("#mask").hide();
    })

//// aqui é o script para abrir o nosso pequeno modal

    $("a[rel=modal]").click(function(ev) {
        ev.preventDefault();
        var id = $(this).attr("href");

        var alturaTela = $(document).height();
        var larguraTela = $(window).width();

        //alert("clicou");

        //colocando o fundo preto
        $('#mask').css({'width': larguraTela, 'height': alturaTela});
        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow", 0.8);

        var left = ($(window).width() / 2) - ($(id).width() / 2);
        var top = ($(window).height() / 2) - ($(id).height() / 2);

        $(id).css({'top': top, 'left': left});
        $(id).show();
        //element = document.getElementById("name");
        //element.focus();
    });

    //$('.close').click(function(ev) {
    //    ev.preventDefault();
    //    $("#mask").hide();
    //    $(".window").hide();
    //    $(".deletewindow").hide();
    //});

});