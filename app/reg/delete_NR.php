<?php
/**
 * Created by PhpStorm.
 * User: Robson
 * Date: 19/11/2019
 * Time: 11:17
 */

require_once '../../users/init.php';
//require_once $abs_us_root.$us_url_root.'users/includes/template/prep.php';

if(isset($user) && $user->isLoggedIn()) { } else { header( "Location: /index.php" ); exit; }
if (!securePage($_SERVER['PHP_SELF'])){die();}

require_once '../functions.php';

$id = $_GET['id'];

    $result = deleteNR($id);

$res = '';

// Violação de chave única
if (strpos($result, ERROR_UNIQUE_ID)) {
    $res = "ERRO: Registro não inserido, nome já existente";
}

echo $res;


?>