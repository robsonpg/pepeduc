<?php
/**
 * Created by PhpStorm.
 * User: Robson
 * Date: 07/10/2019
 * Time: 20:15
 */

require_once '../../users/init.php';

if (!securePage($_SERVER['PHP_SELF'])){die();}
if(isset($user) && $user->isLoggedIn()) { } else { header( "Location: /index.php" ); exit; }

// Verifica se existe a variável txtnome
if (isset($_GET["txtname"])) {

    $name = $_GET["txtname"];

    //echo "Name: " . $name;

    //return $name;
    //exit;

    $db = DB::getInstance();

    // GET USER
    $get_user = $db->query("SELECT * FROM device_clients WHERE LOWER(client_name) like '%" . $name . "%' and client_delete_date is null");
    $get_user_count = $get_user->count();
    $users = $get_user->results();

    // Verifica se a variável está vazia
    /*if (empty($nome)) {
        $sql = "SELECT * FROM contato";
    } else {
        $nome .= "%";
        $sql = "SELECT * FROM contato WHERE nome like '$nome'";
    }*/
    sleep(1);

    // Verifica se a consulta retornou linhas
    if ($get_user_count > 0) {
        // Atribui o código HTML para montar uma tabela
        $inner_table = "<table class=\"table table-bordered\">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>";
        $return = "$inner_table";
        // Captura os dados da consulta e inseri na tabela HTML
        foreach ($users as $item) {
            $return.= "<tr>";
            $return.= "<td>" . $item->client_name . "</td>";
            $return.= "<td>" . $item->client_city . "</td>";
            $return.= "<td>
                        <a class=\"roundcolor\" id='edit' href=\"#window1\" rel=\"modal\"  
                            onclick='editClick(" . $item->idclient . ")'><i class=\"fa fa-pencil\"></i> Edit</a>
                        <a class=\"roundcolor\" id='erase' href=\"#delete_window\" rel=\"modal\"  
                            onclick='deleteClick(" . $item->idclient . ", \"" . $item->client_name . "\", \"" .
                            $item->client_city . "\")'><i class=\"fa fa-trash\"></i> Delete</a>
                    </td>";

            $return.= "</tr>";
        }
        echo $return.="</tbody></table>";
    } else {
        // Se a consulta não retornar nenhum valor, exibi mensagem para o usuário
        echo "Não foram encontrados registros!";
    }
}
?>