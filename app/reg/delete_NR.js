/**
 * Created by robso on 26/02/2021.
 */

var reg_delete_id;

function deleteClick(id, NR_name) {
    //alert("Erase Click!" + id);
    //alert(NR_name);

    reg_delete_id = id;

    var cptitle = document.getElementById("delete_reg_name");
    cptitle.innerHTML = NR_name;
}

$('#confirm_del').click( function () {
    // apaga o registro que foi passado
    var data = "&id=" + reg_delete_id;

    ajaxRequest = $.ajax({
        type: 'GET',
        url: 'reg/delete_NR.php',
        //dataType: "JSON",
        //async: true,
        data: data,
        processData: false,
        contentType: false,
        success: function(response) {
            //location.reload();
            if (response.indexOf("ERRO") !== -1) {
                alert(response);
            } else {
                //alert(response);
                document.getElementById("lbl_del_success").style.display = "block";
                setTimeout(function() {
                    document.getElementById("lbl_del_success").style.display = "none";
                    $("#mask").hide();
                    $("#delete_nr_window").hide();
                    location.reload();
                }, 1000);
                //$("#mask").hide();
                //$("#window_reg").hide();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("Error: " + textStatus + " - " + errorThrown);
            console.log(textStatus, errorThrown);
        }
    });

})


$('#cancel_del').click( function () {
    //alert('Canceled!');
    $("#delete_nr_window").hide();
    $("#mask").hide();
})