<!-- Facebook sharing meta tags (delete if you don't want them) -->
<!-- Be sure to update this in .11 -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta property="og:url"                content="" /> <!-- URL for website (link address) -->
<meta property="og:type"               content="website" /> <!-- type of site -->
<meta property="og:title"              content="PEP Educ" /> <!-- title of site (title of share) -->
<meta property="og:description"        content="Prontuário Eletrônico da Pessoa: Educativo da Equipe de Saúde" /> <!-- description of site (text which appears when sharing) -->
<meta property="og:image"              content="https://pepinteracao.com.br/users/images/logo1.png" /> <!-- URL for preview image -->
