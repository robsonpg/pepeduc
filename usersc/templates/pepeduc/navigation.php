<!-- Header -->
<header>
    <!-- Header desktop -->
    <div class="wrap-menu-header gradient1 trans-0-4">
        <div class="container h-full">
            <div class="wrap_header trans-0-3">
                <!-- Logo -->
                <div class="logo" id="logo_1">
                    <a href="<?=$us_url_root?>index.php">
                        <img src="<?=$us_url_root?>usersc/templates/<?=$settings->template?>/assets/images/icons/logo.png" alt="IMG-LOGO" data-logofixed="<?=$us_url_root?>usersc/templates/<?=$settings->template?>/assets/images/icons/logo2.png">
                    </a>
                </div>

                <!-- Menu -->
                <div class="wrap_menu p-l-45 p-l-0-xl">
                    <nav class="menu">
                        <ul class="main_menu">

                            <?php if (isset($user) && $user->isLoggedIn()) { ?>
                                <li>
                                    <a href="">Tutoriais</a>
                                </li>

                                <li>
                                    <a href="">Aulas Interativas</a>
                                </li>

                                <li>
                                    <a href="">Ambiente de aprendizagem</a>
                                </li>

                                <?php if (hasPerm([1], $user->data()->id)) { ?>
                                    <li>
                                        <a href="<?=$us_url_root?>users/admin.php">Administraçao</a>
                                    </li>
                                <?php } ?>

                                <li>
                                    <a href="<?=$us_url_root?>users/logout.php">Logout</a>
                                </li>
                            <?php } else { ?>
                                <li>
                                    <a href="<?=$us_url_root?>users/login.php" onclick="doLogin()">Login</a>
                                </li>

                                <li>
                                    <a href="">Solicitar registro</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<script>
    function doLogin() {
        var mp = document.getElementById("login_wrapper");
        mp.innerHTML = "<br><br><br><br><br>" + mp.innerHTML;
    }
</script>

<?php

    if(isset($_GET['err'])){
      err("<font color='red'>".$err."</font>");
    }

    if(isset($_GET['msg'])){
      err($msg);
    }
